package com.company;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


public class Main {

    public static void main(String[] args) throws IOException, ParseException {
        Date date = new Date();
       FileReader road = new FileReader("/home/dmolodtsova/IdeaProjects/files/road.csv");
        roadRestrictions(date, road);
    }

    /**
     * Требуется определить, сколько дорожных ограничений действовало в городе на определенную дату.
     * @param date
     * @param road
     * @throws IOException
     */
    public static void roadRestrictions(Date date, FileReader road) throws IOException {
        BufferedReader road1 = new BufferedReader(road);
        String line = "";
        String[] splitted;
        int i = 0;
        Date startDate;
        Date expirationDate;
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy/MM/dd");

        while ((line = road1.readLine()) != null) {

            splitted = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

            try
            {
                String startDateYear = splitted[11].substring(0,4);
                String startDateMonth = splitted[11].substring(4,6);
                String startDateDay = splitted[11].substring(6);
                String startDateString = startDateYear+"/"+startDateMonth+"/"+startDateDay;

                String expirationDateYear = splitted[12].substring(0,4);
                String expirationDateMonth = splitted[12].substring(4,6);
                String expirationDateDay = splitted[12].substring(6);
                String expirationDateString = expirationDateYear+"/"+expirationDateMonth+"/"+expirationDateDay;

                startDate = formatter.parse(startDateString);
                expirationDate = formatter.parse(expirationDateString);

                if (date.after(startDate) && date.before(expirationDate))
                {
                    i++;
                    System.out.printf(" Дата начала: %s - дата окончания: %s \n",formatter.format(startDate),formatter.format(expirationDate));

                }
            }
            catch (Exception e)
            {
                //System.out.println("Некорректные данные  " + e);
            }
        }
        System.out.printf("Дорожных ограничений на дату %s: %s",formatter.format(date),i);

        road1.close();
    }
}
